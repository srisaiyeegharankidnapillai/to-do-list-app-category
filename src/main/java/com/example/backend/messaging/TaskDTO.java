package com.example.backend.messaging;
import com.example.backend.model.Priority;

public class TaskDTO {


	private Long taskId;
	private String taskName;
	private Priority taskPriority;
	private boolean taskStatus;
	private CategoryDTO taskCategory;
	private AppUserDTO taskUser;
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Priority getTaskPriority() {
		return taskPriority;
	}
	public void setTaskPriority(Priority taskPriority) {
		this.taskPriority = taskPriority;
	}
	public boolean isTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(boolean taskStatus) {
		this.taskStatus = taskStatus;
	}
	public CategoryDTO getTaskCategory() {
		return taskCategory;
	}
	public void setTaskCategory(CategoryDTO taskCategory) {
		this.taskCategory = taskCategory;
	}
	public AppUserDTO getTaskUser() {
		return taskUser;
	}
	public void setTaskUser(AppUserDTO taskUser) {
		this.taskUser = taskUser;
	}
	
	
}
